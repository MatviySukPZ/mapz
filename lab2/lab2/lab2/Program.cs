﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtensionMethods;

namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Location> list = new List<Location>();
            Dictionary<string, Location[]> dict = new Dictionary<string, Location[]>();

            list.Add(new Location(2.0, 3.0, "Lviv"));
            list.Add(new Location(1.0, 15.0, "Kyiv"));
            list.Add(new Location(6.0, 7.4, "Ternopil"));
            list.Add(new Location(0.8, 23.0, "Warsaw"));
            list.Add(new Location(9.5, 91.3, "Krakow"));
            list.Add(new Location(0.0, 0.0, "London"));
            list.Add(new Location(74.1, 54.0, "Rome"));
            list.Add(new Location(69.3, 65.0, "Milan"));
            list.Add(new Location(72.4, 80.5, "Barcelona"));
            list.Add(new Location(39.4, 31.0, "Madrid"));

            dict.Add(Location.Ukraine, new Location[]
            {
                new Location(2.0, 3.0, "Lviv"),
                new Location(1.0, 15.0, "Kyiv"),
                new Location(6.0, 7.4, "Ternopil")
            });
            dict.Add(Location.Spain, new Location[]
            {
                new Location(39.4, 31.0, "Madrid"),
                new Location(72.4, 80.5, "Barcelona")
            });
            dict.Add(Location.Poland, new Location[]
            {
                new Location(0.8, 23.0, "Warsaw"),
                new Location(9.5, 91.3, "Krakow")
            });
            dict.Add(Location.GreatBritain, new Location[]
            {
                new Location(0.0, 0.0, "London")
            });
            dict.Add(Location.Italy, new Location[]
            {
                new Location(74.1, 54.0, "Rome"),
                new Location(69.3, 65.0, "Milan")
            });

            // Select --------------------------------------------
            IEnumerable<string> allCities = list.Select(x => x.name);
            Console.WriteLine("Усі міста(Select):");
            foreach(string city in allCities)
            {
                Console.WriteLine(city);
            }

            // Where & ExtendedMethod ----------------------------------------
            IEnumerable<Location> ukraineCities = list.Where(item => item.isZeroLocation());
            Console.WriteLine("\nМіста із нульовими координатами(Where):");
            foreach (Location city in ukraineCities)
            {
                Console.WriteLine(city.name);
            }

            // Dictionary ------------------------------------------------
            IEnumerable<Location> italyCities = dict.First(country => country.Key == Location.Italy).Value;
            Console.WriteLine("\nМіста Італії(Dictionary):");
            foreach (Location city in italyCities)
            {
                Console.WriteLine(city.name);
            }

            // Anonymous types and init
            var coordinates = from city in list
                              select new { x = city.getX(), y = city.getY() };

            Console.WriteLine("\nУсі координати");
            foreach(var point in coordinates)
            {
                Console.WriteLine("Point x: {0}, y: {1}", point.x, point.y);
            }


            // IComparer<Location> -------------------------------------------
            Location[] arrayLocations = list.ToArray();
            Array.Sort(arrayLocations, new LocationComparer());

            Console.WriteLine("\nСортування елементів за зростанням відстані(IComparer): ");
            foreach (Location point in arrayLocations)
            {
                Console.WriteLine(point.name);
            }
        }
    }
}
