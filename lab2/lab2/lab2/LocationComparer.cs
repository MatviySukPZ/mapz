﻿using System;
using System.Collections.Generic;


namespace lab2
{
    public class LocationComparer : IComparer<Location>
    {
        public LocationComparer()
        {
           
        }

        public int Compare(Location loc1, Location loc2)
        {
            return loc1.point().CompareTo(loc2.point());
        }
    }
}
