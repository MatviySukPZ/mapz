﻿using lab2;

namespace ExtensionMethods
{
    public static class LocationExtensions
    {
        public static bool isZeroLocation(this Location location)
        {
            return location.getX() == 0.0 && location.getY() == 0.0;
        }


        // Short name has LESS THAN 6 symbols
        public static bool isShortName(this Location location)
        {
            return location.name.Length < 6;
        } 
    }
}