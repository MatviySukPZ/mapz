﻿using System;
namespace lab2
{
    public class Location
    {
        double x, y;
        public string name;

        static public string Ukraine = "Ukraine";
        static public string Spain = "Spain";
        static public string Poland = "Poland";
        static public string GreatBritain = "GreatBritain";
        static public string Italy = "Italy";


        public Location(double x, double y, string name)
        {
            this.x = x;
            this.y = y;
            this.name = name;
        }

        public double getX()
        {
            return this.x;
        }

        public double getY()
        {
            return this.y;
        }

        public double point()
        {
            return (this.x + this.y) / 1.5;
        }
    }
}
