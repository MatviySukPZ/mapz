﻿using System;
using lab1.Abstracts;
using lab1.Interfaces;

namespace lab1.Classes
{
    class Circle: Shape, IPrint, ILocate
    {
        private double radius;
        public const string name = "Circle";

        public Circle(double radius)
        {
            this.radius = radius;
        }

        void IPrint.Print()
        {
            Console.WriteLine("Circle area: {0}", this.GetArea());
        }

        public override double GetArea()
        {
            return Math.PI * Math.Pow(radius, 2.0);
        }

        public static implicit operator double(Circle circle) => circle.radius;
        public static explicit operator Circle(double radius) => new Circle(radius);
    }
}
