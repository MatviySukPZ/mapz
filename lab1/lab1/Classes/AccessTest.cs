﻿using System;
using lab1.Interfaces;
namespace lab1.Classes
{
    class AccessTest: IAccess
    {
        const int accessTestVariable = 0;

        AccessClass someClass = new AccessClass();

        public AccessTest()
        {
        }

        void accessTestMethod()
        {
            double number = someClass.accessTestVariable;
            Console.WriteLine("Method access allowed!");
        }

        void IAccess.TryAccess()
        {
            Console.WriteLine("Interface access allowed!");
        }

        private class AccessClass
        {
            public double accessTestVariable = 0.0;

        }
    }
}
