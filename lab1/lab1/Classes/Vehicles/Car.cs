﻿using System;
using lab1.Interfaces;
namespace lab1.Classes
{
    public class Car: Vehicle, IMove
    {
        int doorsNumber;

        public Car()
        {
            this.doorsNumber = 5;
        }

        public Car(int doorsNumber)
        {
            this.doorsNumber = doorsNumber;
        }

        public Car(int doorsNumber, int maxSpeed)
        {
            this.doorsNumber = doorsNumber;
        }

        public override void setMaxSpeed(int speed)
        {
            base.setMaxSpeed(
                doorsNumber > 3 ?
                ((int)(speed * 0.9)) :
                speed
                );
        }

        void IMove.Move()
        {
            Console.WriteLine("Car moved!");
        }
    }
}
