﻿using System;
using lab1.Interfaces;
namespace lab1.Classes
{
    public class Vehicle: IMove
    {
        int maxSpeed;
        const int wheelsNumber = 4;
        static int vehiclesCreated = 0;
        static int vehiclesSold;

        static Vehicle()
        {
            vehiclesSold = 0;
        }

        public Vehicle()
        {
            this.maxSpeed = 60;
        }

        public Vehicle(int maxSpeed)
        {
            this.maxSpeed = maxSpeed;
        }

        public virtual void setMaxSpeed(int speed)
        {
            this.maxSpeed = speed;
        }

        void IMove.Move()
        {
            Console.WriteLine("Vehicle moved!");
        }
    }
}
