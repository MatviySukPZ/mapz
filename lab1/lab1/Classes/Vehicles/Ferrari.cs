﻿using System;
using lab1.Interfaces;
namespace lab1.Classes.Vehicles
{
    public class Ferrari: Car, IMove
    {
        public Ferrari()
        {
        }

        public override void setMaxSpeed(int speed)
        {
            base.setMaxSpeed((int)(speed * 1.2));
        }

        void IMove.Move()
        {
            Console.WriteLine("Ferrari moved!");
        }
    }
}
