﻿using System;
namespace lab1.Enums
{
    [Flags]
    public enum SmoothieTypes
    {
        banana, 
        coconut,
        oilAndGas,
        strawberry,
        cucumber,
        kiwi,
        orange
    }
}
