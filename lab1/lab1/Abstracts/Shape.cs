﻿using System;
namespace lab1.Abstracts
{
    abstract class Shape
    {
        public abstract double GetArea();
    }
}
