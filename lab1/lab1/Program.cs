﻿using System;
using lab1.Classes;
using lab1.Classes.Vehicles;
using lab1.Interfaces;
using lab1.Enums;
using lab1.Structs;
using System.Diagnostics;
using System.Threading;

namespace lab1
{
    class Program
    {
        public static void setZero(out int value)
        {
            value = 0;
        }

        public static void increment(ref int value)
        {
            value += 1;
        }

        static void Main(string[] args)
        {
            const int itemsCount = 1000000;
            Vehicle vehicle = new Vehicle();
            Car car = new Car(5);
            Vehicle ferrari = new Ferrari();
            Stopwatch firstTimer = new Stopwatch();
            Stopwatch secTimer = new Stopwatch();

            firstTimer.Start();
            for (int i = 0; i < itemsCount; i++)
            {
                vehicle.setMaxSpeed(100);
                car.setMaxSpeed(100);
                ferrari.setMaxSpeed(100);
            }
            firstTimer.Stop();

            secTimer.Start();
            for (int i = 0; i < itemsCount; i++)
            {
                IMove iVehicle = vehicle as IMove;
                if (iVehicle != null)
                {
                    iVehicle.Move();
                }

                IMove iCar = car as IMove;
                if (iCar != null)
                {
                    iCar.Move();
                }

                IMove iFerrari = ferrari as IMove;
                if (iFerrari != null)
                {
                    iFerrari.Move();
                }
            }
            secTimer.Stop();

            Console.WriteLine("Interface time in miliseconds: {0}", firstTimer.Elapsed.Milliseconds / 10);
            Console.WriteLine("Usual time in miliseconds: {0}", secTimer.Elapsed.Milliseconds / 10);

            /*
            Vehicle veh1 = new Vehicle();


            AccessTest accessTest = new AccessTest();
            IAccess access = accessTest as IAccess;

            if (access != null)
            {
                access.TryAccess();
            }

            Circle circle = new Circle(5.0);

            IPrint printable = circle as IPrint;

            if (printable != null)
            {
                printable.Print();
            } else {
                Console.WriteLine("IPrint interface is not realized");
            }

            // Enums ------------------------------------
            
            SmoothieTypes first = SmoothieTypes.cucumber | SmoothieTypes.coconut; // first = 5
            SmoothieTypes sec = SmoothieTypes.cucumber & SmoothieTypes.coconut; // sec = 0
            SmoothieTypes third = SmoothieTypes.cucumber ^ SmoothieTypes.coconut; // third = 5
            byte fourth = ((byte)SmoothieTypes.cucumber) << 2; // cucumber = 4; fourth = 16
            byte fifth = ((byte)SmoothieTypes.orange) >> 1; // orange = 6; fifth = 3

            Console.WriteLine("{0}", ((byte)fifth));
            

            EmptyClass emptyClass = new EmptyClass();
            Console.WriteLine("Is EmptyClass equal Circle: {0}", emptyClass.Equals(circle));
            Console.WriteLine("EmptyClass hashCode: {0}", emptyClass.GetHashCode());
            Console.WriteLine("EmptyClass Type: {0}", emptyClass.GetType());
            Console.WriteLine("EmptyClass ToString: {0}", emptyClass.ToString());
            */
        }
    }
}
