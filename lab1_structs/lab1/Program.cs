﻿using System;
using lab1.Classes;
using lab1.Interfaces;

namespace lab1
{
    class Program
    {
        public static void setZero(out int value)
        {
            value = 0;
        }

        public static void increment(ref int value)
        {
            value += 1;
        }

        static void Main(string[] args)
        {
            
        }
    }
}
