﻿using System;
using lab1.Interfaces;
namespace lab1.Classes.Vehicles
{
    public struct Ferrari: Car, IMove
    {

        public override void setMaxSpeed(int speed)
        {
            base.setMaxSpeed((int)(speed * 1.2));
        }

        void IMove.Move()
        {
            Console.WriteLine("Ferrari moved!");
        }
    }
}
